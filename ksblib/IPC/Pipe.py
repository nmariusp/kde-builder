import os

import type_enforced
from typing import override

from .IPC import IPC

from ..BuildException import BuildException

# use IO::Handle;
import subprocess


# use Errno qw(EINTR);


class IPC_Pipe(IPC):
    """
    IPC class that uses pipes in addition to forking for IPC.
    """
    
    def __init__(self):
        IPC.__init__(self)
        
        # Define file handles.
        # self.fh = subprocess.Popen([], stdin=subprocess.PIPE, stdout=subprocess.PIPE)  # todo
        self.fh = os.pipe()  # todo
    
    def setSender(self):
        """
        Call this to let the object know it will be the update process.
        """
        self.fh.writer()
        
        # Disable buffering and any possibility of IO 'interpretation' of the bytes
        self.fh.autoflush(1)
        binmode(self.fh)
    
    def setReceiver(self):
        self.fh.reader()
        
        # Disable buffering and any possibility of IO 'interpretation' of the bytes
        self.fh.autoflush(1)
        binmode(self.fh)
    
    @staticmethod
    @override
    def supportsConcurrency() -> bool:
        """
        Reimplementation of ksb::IPC::supportsConcurrency
        """
        return True
    
    @override
    def sendMessage(self, msg):
        """
        Required reimplementation of ksb::IPC::sendMessage
        First parameter is the (encoded) message to send.
        """
        
        # Since streaming does not provide message boundaries, we will insert
        # ourselves, by sending a 2-byte unsigned length, then the message.
        encodedMsg = pack("S a*", length(msg), msg)
        result = self.fh.syswrite(encodedMsg)
        
        if not result or length(encodedMsg) != result:
            BuildException.croak_runtime(f"Unable to write full msg to pipe: {e}")
        
        return 1
    
    def _readNumberOfBytes(self, length):
        fh = self.fh
        readLength = 0
        result = None
        
        while readLength < length:
            err = 0  # Reset errno
            
            curLength = fh.sysread(result, (length - readLength), readLength)
            
            # EINTR is OK, but check early so we don't trip 0-length check
            if curLength is None and err["EINTR"]:
                continue
            if curLength is not None and curLength == 0:
                return
            if not curLength:
                BuildException.croak_internal(f"Error reading $length bytes from pipe: {err}")
            
            if curLength > length:
                BuildException.croak_internal(f"sysread read too much: {curLength} vs {length}")
            
            readLength += curLength
        
        return result
    
    @override
    def receiveMessage(self):
        """
        Required reimplementation of ksb::IPC::receiveMessage
        """
        # Read unsigned short with msg length, then the message
        msgLength = self._readNumberOfBytes(2)
        if not msgLength:
            return
        
        msgLength = unpack("S", msgLength)  # Decode to Perl type
        if not msgLength:
            BuildException.croak_internal(f"Failed to read {msgLength} bytes as needed by earlier message!")
        
        return self._readNumberOfBytes(msgLength)
    
    @override
    def close(self):
        self.fh.close()
