# KDE Builder

This script streamlines the process of setting up and maintaining a development
environment for KDE software.

It does this by automating the process of downloading source code from the
KDE source code repositories, building that source code, and installing it
to your local system.

## Installation

This project it targeting python version 3.12.

Install python 3.12. For example in Arch Linux you will run (assuming you use yay):

```bash
$ yay -S python312
```

Install pipenv. In Arch Linux the command will be:

```bash
$ sudo pacman -S python-pipenv 
```

Open the terminal in the directory where you would store the project and clone it:

```bash
$ cd ~/Development
$ git clone git@invent.kde.org:ashark/kde-builder.git
$ cd kde-builder
```

Create a virtual environment with the required packages:

```bash
$ pipenv install
```

Now you can run the script like this: 

```bash
$ pipenv run python kde-builder --pretend kcalc
```

For convenience, you can create a wrapper script.
Create a file `~/bin/kde-builder` (assuming the `~/bin` is
in your PATH), make this file executable. Add the following content to it:

```bash
#!/bin/bash

cd ~/Development/kde-builder
pipenv run kde-builder $@
```

Now you can invoke the script like this:

```bash
$ kde-builder --pretend kcalc
```

## Documentation

See the wiki page [Get_Involved/development](https://community.kde.org/Get_Involved/development).
It covers usage of `kdesrc-build` script - a perl predecessor of `kde-builder`.

For more details, consult the project documentation. The most important pages are:

- [List of supported configuration options](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/conf-options-table.html)
- [Supported command line parameters](https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/supported-cmdline-params.html)
